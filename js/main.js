;(function() {

    /**
     * Sidebar dropdown behaviour
     */

     $('.sidebarDrpdwn').each(function(index, drpdwn){

        // Toggle opening and closing
         $(drpdwn).find('a.header').click(function() {
            $(this).parent().find('.drpdwn-content').slideToggle('fast');
            $(this).parent().find('.drpdwn-toggle').toggleClass('fa-chevron-up fa-chevron-down');
         });
     });
}());